# Tesla ChatBot K8S Deploy Tutorial

## 部署流程 1: 将AWS S3中的docker images下载到客户环境中
```shell
wget -c https://tesla-chatbot.s3.cn-north-1.amazonaws.com.cn/tesla-chatbot-0327.tar.gz
```

## 部署流程 2: 将上述image导入客户环境的镜像仓库中
```shell
# 加载 docker images
gunzip -c tesla-chatbot-0327.tar.gz | docker load
# docker push 到镜像仓库
docker push hub.docker??.com/tesla/tesla-chatbot:latest
```

## 部署流程 3: 将yaml文件中对应变量换成客户环境中的数据
参照下述附注内容

## 部署流程 4: k8s中部署yaml文件
```shell
kubectl apply xxx.yaml
```

## 部署流程 5: 测试访问
开放的是5000端口 访问 http://xxx:5000/ 即可 

## 附: k8s yaml 文件
```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations: {}
  labels:
    k8s.kuboard.cn/layer: web
    k8s.kuboard.cn/name: chatbot
  name: chatbot
  namespace: tesla-chatbot
  resourceVersion: '3199338'
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 5
  selector:
    matchLabels:
      k8s.kuboard.cn/layer: web
      k8s.kuboard.cn/name: chatbot
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      creationTimestamp: null
      labels:
        k8s.kuboard.cn/layer: web
        k8s.kuboard.cn/name: chatbot
    spec:
      affinity: {}
      containers:
        - env:
            - name: AWS_ES_URL
              value: >-
                https://search-eccom-02-cgm2fztucf2sdrhvenuhwehh3q.cn-north-1.es.amazonaws.com.cn
            - name: AWS_ES_USERNAME
              value: guzhiqing1
            - name: AWS_ES_PASSWORD
              value: kFI1Z5=r1
            - name: AWS_BOT_ENDPOINT
              value: >-
                https://1kh9r0j7c4.execute-api.cn-north-1.amazonaws.com.cn/v1/bot
          image: registry.xxxxxxx.com/tesla-chatbot/tesla-chatbot:v202303201413
          imagePullPolicy: Always
          name: chatbot-container
          ports:
            - containerPort: 5000
              hostPort: 5000
              protocol: TCP
          resources: {}
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30

---
apiVersion: v1
kind: Service
metadata:
  annotations: {}
  labels:
    k8s.kuboard.cn/layer: web
    k8s.kuboard.cn/name: chatbot
  name: chatbot
  namespace: tesla-chatbot
  resourceVersion: '2846437'
spec:
  clusterIP: 10.233.109.41
  clusterIPs:
    - 10.233.109.41
  internalTrafficPolicy: Cluster
  ipFamilies:
    - IPv4
  ipFamilyPolicy: SingleStack
  ports:
    - name: 8y6nwe
      port: 5000
      protocol: TCP
      targetPort: 5000
  selector:
    k8s.kuboard.cn/layer: web
    k8s.kuboard.cn/name: chatbot
  sessionAffinity: ClientIP
  sessionAffinityConfig:
    clientIP:
      timeoutSeconds: 10800
  type: ClusterIP

---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations: {}
  labels:
    k8s.kuboard.cn/layer: web
    k8s.kuboard.cn/name: chatbot
  name: chatbot
  namespace: tesla-chatbot
  resourceVersion: '3015948'
spec:
  ingressClassName: nginx
  rules:
    - host: www.tesla-chatbot.com
      http:
        paths:
          - backend:
              service:
                name: chatbot
                port:
                  number: 5000
            path: /
            pathType: Prefix
```

## 注: yaml文件中需要修改的位置
```shell
AWS_ES_URL        # AWS ES 链接地址
AWS_ES_USERNAME   # AWS ES 用户名
AWS_ES_PASSWORD   # AWS ES 密码
AWS_BOT_ENDPOINT  # AWS ChatBot 链接地址
registry.xxxxxxx.com/tesla-chatbot/tesla-chatbot:v202303201413  # 镜像地址
www.tesla-chatbot.com  # ingress 入口域名
```


```shell
# AWS CLI 安装
https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
# 配置环境变量
mkdir ~/.aws
vi ~/.aws/credentials
# 添加如下代码
[default]
aws_access_key_id = xxx
aws_secret_access_key = xxx
# 验证
aws iam list-users
# 导出镜像
# docker save tesla-chatbot|gzip > tesla-chatbot-0324.tar.gz
# 上传到 aws s3
# aws s3 cp tesla-chatbot-0326.tar.gz s3://tesla-chatbot/

aws s3 cp tesla_chatbot-source-0327.tar.gz s3://tesla-chatbot/
```