FROM tiangolo/uwsgi-nginx-flask:python3.9

ENV  TIME_ZONE Asia/Shanghai
# 镜像源地址一
ENV PIPURL "https://pypi.tuna.tsinghua.edu.cn/simple"
ENV PIPHOST "pypi.tuna.tsinghua.edu.cn"
# 镜像源地址二
#ENV PIPURL "http://mirrors.aliyun.com/pypi/simple"
#ENV PIPHOST "mirrors.aliyun.com"

RUN echo "${TIME_ZONE}" > /etc/timezone \
    && ln -sf /usr/share/zoneinfo/${TIME_ZONE} /etc/localtime

ENV PIP_ROOT_USER_ACTION=ignore

#COPY . /app
#COPY ./flask_session /app/flask_session
COPY ./static /app/static
COPY ./templates /app/templates
COPY ./utils /app/utils
COPY ./app.py /app/app.py
COPY ./app_config.py /app/app_config.py
COPY ./env.sh /app/env.sh
COPY ./requirements.txt /app/requirements.txt
COPY ./aws_bot_demo.py /app/aws_bot_demo.py

WORKDIR /app

# set mirrors source
RUN pip config set global.index-url ${PIPURL} \
    && pip config set install.trusted-host ${PIPHOST}

# upgrade pip
RUN pip install --upgrade pip

# install requirements.txt
RUN pip install -r requirements.txt -i ${PIPURL}

# install defined libs
RUN pip install flask-cors elasticsearch boto3 botocore requests_aws4auth pyopenssl flask_sslify\
    && chmod +x env.sh

CMD export FLASK_ENV=production
CMD export FLASK_DEBUG=False
# 启动项目
CMD ["sh", "./env.sh"]
