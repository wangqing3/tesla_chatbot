#!/bin/sh

# 定义变量
AWS_ECR_REGISTRY=007261106368.dkr.ecr.cn-north-1.amazonaws.com.cn
IMAGE_NAME=meadhu/tesla-chatbot
VERSION=date +%Y%m%d%H%M%S


# 1. 检索身份验证令牌并向注册表验证 Docker 客户端身份。
# aws ecr get-login-password --region cn-north-1 | docker login --username AWS --password-stdin 007261106368.dkr.ecr.cn-north-1.amazonaws.com.cn
aws ecr get-login-password --region cn-north-1 | docker login --username AWS --password-stdin ${AWS_ECR_REGISTRY}

# 2. 使用以下命令生成 Docker 映像
# docker build -t eccom/tesla-chatbot:v03271200 .
docker build -t ${IMAGE_NAME}:${VERSION} .

# 3. 生成完成后，标记您的映像，以便将映像推送到此存储库:
# docker tag eccom/tesla-chatbot:v03271200 007261106368.dkr.ecr.cn-north-1.amazonaws.com.cn/eccom/tesla-chatbot:v03271200
docker tag ${IMAGE_NAME}:${VERSION} ${AWS_ECR_REGISTRY}/${IMAGE_NAME}:${VERSION}

# 4. 运行以下命令将此映像推送到您新创建的 亚马逊云科技 存储库
# docker push 007261106368.dkr.ecr.cn-north-1.amazonaws.com.cn/eccom/tesla-chatbot:v03271200
docker push ${AWS_ECR_REGISTRY}/${IMAGE_NAME}:${VERSION}