from elasticsearch import Elasticsearch
# from opensearchpy import OpenSearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth





class ElasticOption():
	def __init__(self):
		self.es = Elasticsearch(
			# 'http://10.200.30.127:9201',  # 连接集群，以列表的形式存放各节点的IP地址
			'http://10.200.90.135:9201',  # 连接集群，以列表的形式存放各节点的IP地址
		)
		self.index_name = "conversation"
		# self.mappings = {
		# 	"properties": {
		# 		"displayName": {
		# 			"type": "text"
		# 		},
		# 		"objectId": {
		# 			"type": "text"
		# 		},
		# 		"tenantId": {
		# 			"type": "text"
		# 		},
		# 		"preferredUserName": {
		# 			"type": "text"
		# 		},
		# 		"question": {
		# 			"type": "text"
		# 		},
		# 		"answer": {
		# 			"type": "text"
		# 		},
		# 		"date": {
		# 			"type": "text"
		# 		}
		# 	}
		# }
		
		self.mappings = {
			"mappings": {
				"properties": {
					"displayName": {
						"type": "text"
					},
					"objectId": {
						"type": "text"
					},
					"tenantId": {
						"type": "text"
					},
					"preferredUserName": {
						"type": "text"
					},
					"question": {
						"type": "text"
					},
					"answer": {
						"type": "text"
					},
					"date": {
						"type": "date",
						"format": "yyyy-MM-dd HH:mm:ss"
					},
					"createTime": {
						"type": "date",
						"format": "yyyy-MM-dd HH:mm:ss"
					},
					"updateTime": {
						"type": "date",
						"format": "yyyy-MM-dd HH:mm:ss"
					}
				}
				
			}
		}
		self.creat_index()
	
	def creat_index(self):
		# 判断index是否存在, 不存在则创建并设置settings
		if not self.es.indices.exists(index=self.index_name):
			self.es.indices.create(index=self.index_name, body=self.mappings)
	
	def add_conversation(self, data: dict):
		"""
		向conversation索引中添加
		:param data:
		:return:
		"""
		result = self.es.index(index=self.index_name, body=data)
		return result
	
	def query_conversation(self,query_data,size=5):
		"""
		查询该用户近5条数据（按日期倒序取近5条）
		:param query_data: 用户信息
		:param size: 查询数量
		:return:
		"""
		body = {
			"query": {
				"bool": {
					"must": [
						{
							"match": {
								"objectId": query_data
							}
						},
						{
							"match_all": {}
						}
					]
				}
			},
			"sort": [
				{"date": {"order": "desc", "format": "yyyy-MM-dd HH:mm:ss"}}
			],
			"size": size
		}
		result = self.es.search(index=self.index_name, body=body)
		data_list_limit5 = [hit['_source'] for hit in result['hits']['hits']]
		return data_list_limit5

# result = es.get(index="new1")
# print(result)
