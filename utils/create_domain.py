import boto3
import botocore
from botocore.config import Config
import time

# Build the client using the default credential configuration.
# You can use the CLI and run 'aws configure' to set access key, secret
# key, and default region.

my_config = Config(
	# Optionally lets you specify a region other than your default.
	region_name='eccom-poc'
)

client = boto3.client('opensearch', config=my_config)

domainName = 'eccom-poc'  # The name of the domain


def createDomain(client, domainName):
	"""Creates an Amazon OpenSearch Service domain with the specified options."""
	response = client.create_domain(
		DomainName=domainName,
		EngineVersion='OpenSearch_1.0',
		ClusterConfig={
			'InstanceType': 't2.small.search',
			'InstanceCount': 5,
			'DedicatedMasterEnabled': True,
			'DedicatedMasterType': 't2.small.search',
			'DedicatedMasterCount': 3
		},
		# Many instance types require EBS storage.
		EBSOptions={
			'EBSEnabled': True,
			'VolumeType': 'gp2',
			'VolumeSize': 10
		},
		AccessPolicies="{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"AWS\":[\"arn:aws:iam::123456789012:user/user-name\"]},\"Action\":[\"es:*\"],\"Resource\":\"arn:aws:es:us-west-2:123456789012:domain/my-test-domain/*\"}]}",
		NodeToNodeEncryptionOptions={
			'Enabled': True
		}
	)
	print("Creating domain...")
	print(response)
