# from opensearchpy import OpenSearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth
import boto3


# class AmazonOpensearchService:
# 	def __init__(self):
# 		self.host = ''
# 		self.region = ''
# 		service = 'es'
# 		credentials = boto3.Session().get_credentials()
# 		self.awsauth = AWS4Auth(
# 			credentials.access_key, credentials.secret_key, self.region, service,
# 			session_token=credentials.token)
# 		self.client = self.create_OpenSearch_connection()
#
# 	def create_OpenSearch_connection(self):
# 		client = OpenSearch(
# 			hosts=[{'host': self.host, 'port': 443}],
# 			http_auth=self.awsauth,
# 			use_ssl=True,
# 			verify_certs=True,
# 			connection_class=RequestsHttpConnection
# 		)
# 		return client




# class AmazonOpenSearchService:
#
#     def __init__(self, endpoint, region, index_name, doc_type):
#         self.opensearch = boto3.client('opensearch', endpoint_url=endpoint, region_name=region)
#         self.index_name = index_name
#         self.doc_type = doc_type
#
#     def create_index(self):
#         """创建索引"""
#         body = {
#             'settings': {
#                 'index': {
#                     'number_of_shards': 1,
#                     'number_of_replicas': 0
#                 }
#             },
#             'mappings': {
#                 self.doc_type: {
#                     'properties': {
#                         'title': {'type': 'text'},
#                         'body': {'type': 'text'},
#                         'timestamp': {'type': 'date'}
#                     }
#                 }
#             }
#         }
#         self.opensearch.indices.create(index=self.index_name, body=body)
#
#     def put_document(self, document):
#         """存入数据"""
#         response = self.opensearch.index(
#             index=self.index_name,
#             doc_type=self.doc_type,
#             body=document
#         )
#         return response
#
#     def search(self, query):
#         """查询数据"""
#         response = self.opensearch.search(
#             index=self.index_name,
#             body={'query': {'query_string': {'query': query}}}
#         )
#         return response['hits']['hits']